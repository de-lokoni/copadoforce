<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>copado__User_Story__c</defaultLandingTab>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <label>Copado Experience</label>
    <logo>copado__Copado/copado__Copado_Logo.png</logo>
    <tabs>copado__Project__c</tabs>
    <tabs>copado__User_Story__c</tabs>
    <tabs>copado__Promotion__c</tabs>
    <tabs>copado__Deployment__c</tabs>
    <tabs>copado__Environment__c</tabs>
    <tabs>copado__Deployment_Flow__c</tabs>
    <tabs>copado__Copado_DX_Panel</tabs>
    <tabs>copado__Artifact__c</tabs>
    <tabs>copado__Compliance_Rule__c</tabs>
    <tabs>copado__Work_Manager</tabs>
    <tabs>copado__Kanban_Board__c</tabs>
    <tabs>copado__Sprint__c</tabs>
    <tabs>copado__Org__c</tabs>
    <tabs>copado__Snapshot_Difference__c</tabs>
    <tabs>copado__Git_Backup__c</tabs>
    <tabs>copado__Git_Repository__c</tabs>
    <tabs>copado__User_Story_Metadata__c</tabs>
</CustomApplication>
