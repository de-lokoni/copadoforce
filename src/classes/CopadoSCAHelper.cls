/**
 * Created by kheidt.
 */

public with sharing class CopadoSCAHelper {

    /**
     * This method is supposed to run after commits. The execution order in Copado in this case is:
     * Click on "Commit changes" --> SFDC creates Snapshot commit --> Heroku updates snapshot commit -->
     * SFDC creates User Story Commit --> Heroku creates aggregated attachment on story
     *
     * As a result, unless this method is launched through the Attachment trigger (no way), we need to consider 2 sources
     * of information, from where we know which classes need to run: the attachment on the current commit and the
     * attachment on the user story.
     *
     * @param usIds
     */

    @InvocableMethod
    public static void runStaticCodeAnalysisOnUserStory(List<String> usIds){
        //get the user story to gather the required parameters

        if(!usIds.isEmpty() || usIds != null) {
            String usId = usIds[0];

            //get the story record, as we need more details from it
            copado__User_Story__c us = CopadoUserStoryService.getUserStory(usId);

            //get the snapshot commit id, as we need it to check for the attachment in the current commit
            CopadoSnapshotCommitService cscs = new CopadoSnapshotCommitService();
            String snapshotCommitId = cscs.getLatestCommitOnStory(usId);

            CopadoAttachmentService cas = new CopadoAttachmentService();

            //collections for later usage
            List<String> classesFromSnapshotAttachments = new List<String>();
            List<String> classesFromUserStoryAttachments = new List<String>();
            Set<String> uniqueClasses = new Set<String>(classesFromSnapshotAttachments);

            //get attachment from last/current snapshot commit
            classesFromSnapshotAttachments = cas.getApexClassesFromSnapshotCommitAttachments(snapshotCommitId); //put in the snapshot commit id

            /**
             * Stop the operation, if the current ongoing commit does not contain any apex class.
             * If the current commit has no classes, no SCA run is needed and we can safe resources and irrelevant
             * scan results.
             */
            if(snapshotCommitId != null && classesFromSnapshotAttachments == null){
                //throw new CopadoAddOnException('There are no classes in your snapshot attachment');
                System.debug(LoggingLevel.INFO, 'There are no classes in your snapshot attachment');
                return ;
            } else{
                uniqueClasses.addAll(classesFromSnapshotAttachments);
            }

            /**
             * If classes are currently committed we should also run the SCA for the classes, which are already on
             * the story. To do so, we need to query the Git Metadata attachment on the story.
             */
            classesFromUserStoryAttachments = cas.getApexClassesFromUserStoryAttachments(usId);
            if(classesFromUserStoryAttachments == null){
                System.debug(LoggingLevel.INFO, 'No attachment on story found at the moment of transaction.');
            }else {
                uniqueClasses.addAll(classesFromUserStoryAttachments);
            }

            //use the names to prepare the webhook body
            if(!uniqueClasses.isEmpty()) {

                System.debug('classes to be run through scan: ' + uniqueClasses);

                String webhookURL = constructWebhookURL(us);

                String webhookBody = JSON.serialize(new List<String>(uniqueClasses));

                //convert body into required format
                webhookBody = '{ \"gitMetadata\":' + webhookBody + ',\"metadata\":' + webhookBody + '}';
                System.debug('body: ' + webhookBody);

                fireWebhook('POST', webhookURL, webhookBody);
            } else{
                //throw new CopadoAddOnException('THERE IS NO CLASS FOUND, SO NO SCA RUN NEEDED');
                System.debug(LoggingLevel.INFO, 'THERE IS NO CLASS FOUND, SO NO SCA RUN NEEDED');
            }
        } else{
            //throw new CopadoAddOnException('NO USER STORY PASSED TO STORY');
            System.debug(LoggingLevel.INFO, 'NO USER STORY PASSED TO STORY');
        }
    }

    @Future(callout=true)
    public static void fireWebhook(String type, String webhookURL, String webhookBody){
        CopadoWebhookService.callWebhook(type, webhookURL, webhookBody);
    }

    private static String constructWebhookURL(copado__User_Story__c us){

        //construct URL parameters
        String apiParameter = 'api_key=' + Constants_Core.apiKey;
        String usIdParameter = 'userStoryId=' + us.Id;
        String orgCredParameter = us.copado__Org_Credential__c;
        String settingsIdParameter = 'settingsId=' + us.copado__Project__r.copado__Deployment_Flow__r.copado__Static_Code_Analysis_Settings__c;
        String repoParameter = 'repositoryId=' + us.copado__Project__r.copado__Deployment_Flow__r.copado__Git_Repository__c;
        String branchNameParameter = 'branch=feature/' + us.Name;

        //Construct Endpoint URL
        String webhookURL = Constants_Core.webhookBase + Constants_SCA.analyseStaticCodeBase;
        webhookURL += orgCredParameter;
        webhookURL += '?' + apiParameter;
        webhookURL += '&' + usIdParameter;
        webhookURL += '&' + repoParameter;
        webhookURL += '&' + branchNameParameter;
        webhookURL += '&' + settingsIdParameter;

        System.debug('endpoint: ' + webhookURL);

        return webhookURL;
    }
}