/**
 * Created by kheidt on 01/10/2018.
 */

public with sharing class CopadoPromotionTH {

    public void createPromotionsForLowerEnvironments(copado__Promotion__c promotion, copado__Promotion__c oldPromotion){
        if(
                promotion.copado__Status__c == Constants_Core.successfulPromotionStatus &&
                oldPromotion.copado__Status__c != Constants_Core.successfulPromotionStatus
        ){
            CopadoAutoBackPromoteService cabps = new CopadoAutoBackPromoteService(promotion);
            cabps.createBackPromotionsForLowerOrgs();
        }
    }

}