/**
 * Created by kheidt on 2019-06-03.
 */

@IsTest
private class CopadoMassBackPromoteHandlerTest {

    private static copado__Org__c sourceOrgCredential1;
    private static copado__Org__c sourceOrgCredential2;
    private static copado__Org__c sourceOrgCredential3;
    private static copado__Org__c stageOrgCredential;
    private static copado__Org__c uatOrgCredential;
    private static copado__Org__c scratchSourceCredential;
    private static copado__Org__c prodOrgCredential;
    private static copado__Environment__c sourceEnv1;
    private static copado__Environment__c sourceEnv2;
    private static copado__Environment__c sourceEnv3;
    private static copado__Environment__c stageEnv;
    private static copado__Environment__c uatEnv;
    private static copado__Environment__c scratchSource;
    private static copado__Environment__c prodEnv;
    private static copado__Git_Repository__c repo;
    private static copado__Static_Code_Analysis_Settings__c scas;
    private static List<copado__Static_Code_Analysis_Rule__c> scars;
    private static copado__Deployment_Flow__c flow;
    private static copado__Project__c project;
    private static CopadoKeys__c oldProjectId;
    private static copado__User_Story__c userStory1;
    private static copado__User_Story__c userStory2;
    private static copado__User_Story__c userStory3;
    private static copado__User_Story__c userStory4;
    private static copado__User_Story__c scratchSourceStory;
    private static copado__User_Story__c prodEnvStory;
    private static Attachment att1;
    private static Attachment att2;
    private static Attachment att3;
    private static Attachment att4;
    private static Attachment att5;
    private static Attachment att6;
    private static User runningUser;
    private static User adminUser;
    private static Id copadoPermissionSet;
    private static copado.GlobalAPI.UserLicense restoreUser;
    private static CopadoLicenseUtility clu;
    private static Set<Id> excludeEnvironments = new Set<Id>();


    private static void setUp(){
        //create admin user for creating running user
        adminUser = CopadoTestDataFactory.createAdminUser();
        insert adminUser;

        System.runAs(adminUser){
            //create running user
            runningUser = CopadoTestDataFactory.createStandardUser();
            runningUser.ProfileId = adminUser.ProfileId;
            insert runningUser;

            //get copado permission set
            copadoPermissionSet = CopadoTestDataFactory.getCopadoUserPermissionSet().Id;

            //assign permission set to user
            PermissionSetAssignment psa = new PermissionSetAssignment
                    (
                            PermissionSetId = copadoPermissionSet,
                            AssigneeId = runningUser.Id
                    );
            insert psa;

            //assign copado licenses to running User
            clu = new CopadoLicenseUtility();

            //store the Id of the old user, as we need it to restore licenses later, if required.
            //it can be null. if null, an existing non used license was used.
            restoreUser = clu.assignLicenseForTestRun(runningUser.Id);
        }

        System.runAs(runningUser){

            List<copado__Environment__c> envs = new List<copado__Environment__c>();
            sourceEnv1 = CopadoTestDataFactory.createEnvironment();
            sourceEnv1.copado__Org_ID__c = sourceEnv1.copado__Org_ID__c.left(17) + 'W';
            sourceEnv1.Name = 'dev1';
            envs.add(sourceEnv1);

            sourceEnv2 = CopadoTestDataFactory.createEnvironment();
            sourceEnv2.copado__Org_ID__c = sourceEnv2.copado__Org_ID__c.left(17) + 'X';
            sourceEnv2.Name = 'dev2';
            envs.add(sourceEnv2);

            sourceEnv3 = CopadoTestDataFactory.createEnvironment();
            sourceEnv3.copado__Org_ID__c = sourceEnv3.copado__Org_ID__c.left(17) + 'Y';
            sourceEnv3.Name = 'dev3';
            envs.add(sourceEnv3);

            stageEnv = CopadoTestDataFactory.createEnvironment();
            stageEnv.copado__Org_ID__c = stageEnv.copado__Org_ID__c.left(17) + 'A';
            stageEnv.Name = 'Stage';
            envs.add(stageEnv);

            uatEnv = CopadoTestDataFactory.createEnvironment();
            uatEnv.copado__Org_ID__c = uatEnv.copado__Org_ID__c.left(17) + 'B';
            uatEnv.Name = 'UserAcceptanceTest';
            envs.add(uatEnv);


            scratchSource = CopadoTestDataFactory.createEnvironment();
            scratchSource.copado__Org_ID__c = uatEnv.copado__Org_ID__c.left(17) + 'C';
            scratchSource.Name = 'devScratch';
            envs.add(scratchSource);


            prodEnv = CopadoTestDataFactory.createEnvironment();
            prodEnv.copado__Org_ID__c = uatEnv.copado__Org_ID__c.left(17) + 'D';
            prodEnv.Name = 'Prod';
            envs.add(prodEnv);

            upsert envs;

            List<copado__Org__c> creds = new List<copado__Org__c>();
            sourceOrgCredential1 = CopadoTestDataFactory.createOrgCredential(sourceEnv1);
            sourceOrgCredential1.Name = 'dev1';
            sourceOrgCredential2 = CopadoTestDataFactory.createOrgCredential(sourceEnv2);
            sourceOrgCredential2.Name = 'dev2';
            sourceOrgCredential3 = CopadoTestDataFactory.createOrgCredential(sourceEnv3);
            sourceOrgCredential3.Name = 'dev3';
            stageOrgCredential = CopadoTestDataFactory.createOrgCredential(stageEnv);
            stageOrgCredential.Name = 'stage';
            uatOrgCredential = CopadoTestDataFactory.createOrgCredential(uatEnv);
            uatOrgCredential.Name = 'UserAcceptanceTest';
            scratchSourceCredential = CopadoTestDataFactory.createOrgCredential(scratchSource);
            scratchSourceCredential.Name = 'scratch';
            prodOrgCredential = CopadoTestDataFactory.createOrgCredential(prodEnv);
            prodOrgCredential.Name = 'prod';

            creds.add(sourceOrgCredential1);
            creds.add(sourceOrgCredential2);
            creds.add(sourceOrgCredential3);
            creds.add(stageOrgCredential);
            creds.add(uatOrgCredential);
            creds.add(scratchSourceCredential);
            creds.add(prodOrgCredential);
            upsert creds;

            repo = CopadoTestDataFactory.createGitRepo();
            insert repo;

            scas = CopadoTestDataFactory.createStaticCodeAnalysisSettings();
            insert scas;

            scars = new List<copado__Static_Code_Analysis_Rule__c>();
            scars.add(CopadoTestDataFactory.createStaticCodeAnalysisRule(scas));
            insert  scars;

            flow = CopadoTestDataFactory.createDeploymentFlow(scas,repo);
            flow.copado__Active__c = false;
            insert flow;


            flow.copado__Active__c = true;
            system.debug(flow);
            update flow;

            List<copado__Deployment_Flow_Step__c> steps = new List<copado__Deployment_Flow_Step__c>();
            copado__Deployment_Flow_Step__c dfs1 = CopadoTestDataFactory.createDeploymentFlowStep(flow.Id,sourceEnv1.Id, uatEnv.Id);
            dfs1.copado__Branch__c = 'dev1';
            dfs1.copado__Destination_Branch__c = 'uat';

            copado__Deployment_Flow_Step__c dfs2 = CopadoTestDataFactory.createDeploymentFlowStep(flow.Id,sourceEnv2.Id, uatEnv.Id);
            dfs2.copado__Branch__c = 'dev2';
            dfs2.copado__Destination_Branch__c = 'uat';

            copado__Deployment_Flow_Step__c dfs3 = CopadoTestDataFactory.createDeploymentFlowStep(flow.Id,sourceEnv3.Id, uatEnv.Id);
            dfs3.copado__Branch__c = 'dev3';
            dfs3.copado__Destination_Branch__c = 'uat';

            copado__Deployment_Flow_Step__c dfs4 = CopadoTestDataFactory.createDeploymentFlowStep(flow.Id, uatEnv.Id, stageEnv.Id);
            dfs4.copado__Branch__c = 'uat';
            dfs4.copado__Destination_Branch__c = 'stage';

            copado__Deployment_Flow_Step__c dfs5 = CopadoTestDataFactory.createDeploymentFlowStep(flow.Id, stageEnv.Id, prodEnv.Id);
            dfs5.copado__Branch__c = 'stage';
            dfs5.copado__Destination_Branch__c = 'prod';

            copado__Deployment_Flow_Step__c dfs6 = CopadoTestDataFactory.createDeploymentFlowStep(flow.Id, scratchSource.Id,uatEnv.Id);
            dfs6.copado__Branch__c = 'scratch';
            dfs6.copado__Destination_Branch__c = 'uat';

            steps.add(dfs1);
            steps.add(dfs2);
            steps.add(dfs3);
            steps.add(dfs4);
            steps.add(dfs5);
            steps.add(dfs6);
            upsert steps;

            project = CopadoTestDataFactory.createProject(flow);

            if(CopadoTestDataFactory.hasCopadoIntegration){
                // create setting record
                SObject setting = CopadoTestDataFactory.createIntegrationSetting();
                Type sType = Type.forName('Copado_Integration_Setting__c');
                Database.insertImmediate(setting);

                project = CopadoTestDataFactory.addIntegrationSetting(project, (String) setting.Id);

                insert project;
            } else{
                insert project;
            }

            List<copado__User_Story__c> stories = new List<copado__User_Story__c>();

            //stories successfully forward promoted to uat
            userStory1 = CopadoTestDataFactory.createUserStory(project, uatEnv, uatOrgCredential);
            userStory1.copado__Promote_Change__c = true;
            stories.add(userStory1);

            userStory2 = CopadoTestDataFactory.createUserStory(project, uatEnv, uatOrgCredential);
            userStory2.copado__Promote_Change__c = true;
            stories.add(userStory2);

            userStory3 = CopadoTestDataFactory.createUserStory(project, uatEnv, uatOrgCredential);
            userStory3.copado__Promote_Change__c = true;
            stories.add(userStory3);

            //story successfully back promoted to uat
            userStory4 = CopadoTestDataFactory.createUserStory(project, uatEnv, uatOrgCredential);
            userStory4.copado__Promote_Change__c = true;
            stories.add(userStory4);

            //stories sitting in other orgs
            //validation failed scratch to uat
            scratchSourceStory = CopadoTestDataFactory.createUserStory(project, scratchSource, scratchSourceCredential);
            scratchSourceStory.copado__Promote_Change__c = false;
            stories.add(scratchSourceStory);

            //back validation successful stage to uat, successful back promotion prod to stage
            prodEnvStory = CopadoTestDataFactory.createUserStory(project, stageEnv, stageOrgCredential);
            prodEnvStory.copado__Promote_Change__c = true;
            stories.add(prodEnvStory);

            upsert stories;

            List<Attachment> attachments = new List<Attachment>();
            att1 = CopadoTestDataFactory.createUserStoryGitAttachment(userStory1.Id);
            att2 = CopadoTestDataFactory.createUserStoryGitAttachment(userStory2.Id);
            att3 = CopadoTestDataFactory.createUserStoryGitAttachment(userStory3.Id);
            att4 = CopadoTestDataFactory.createUserStoryGitAttachment(userStory4.Id);
            att5 = CopadoTestDataFactory.createUserStoryGitAttachment(scratchSourceStory.Id);
            att6 = CopadoTestDataFactory.createUserStoryGitAttachment(prodEnvStory.Id);

            attachments.add(att1);
            attachments.add(att2);
            attachments.add(att3);
            attachments.add(att4);
            attachments.add(att5);
            attachments.add(att6);
            insert attachments;

            if(CopadoKeys__c.getInstance('CopadoApiKey') == null) {
                CopadoKeys__c cap = CopadoTestDataFactory.createApiKeyCustomSetting();
                insert cap;
            }

            if(CopadoKeys__c.getInstance('ProjectId') == null) {
                CopadoKeys__c cpk = CopadoTestDataFactory.createProjectIdCustomSetting(String.valueOf(project.Id));
                insert cpk;
            } else{
                oldProjectId = CopadoKeys__c.getInstance('ProjectId');
                oldProjectId.Value__c = project.Id;
                update oldProjectId;
            }
        }
    }

    static testMethod void automaticMassBackPromotionCreation() {
        setUp();

        CopadoWebhookCalloutMock calloutMock = new CopadoWebhookCalloutMock();
        calloutMock.mockCreatePromotionAndDeployCall();
        Test.setMock(HttpCalloutMock.class, calloutMock);

        System.debug(calloutMock.responseStatus);

        System.runAs(runningUser) {

            //create promotions
            //successful forward promotions
            List<copado__Promotion__c> successfulPromotions = new List<copado__Promotion__c>();
            copado__Promotion__c dev1uat = CopadoTestDataFactory.createForwardPromotion(project.Id, sourceEnv1.Id, uatEnv.Id);
            dev1uat.copado__Status__c = Constants_Core.successfulPromotionStatus;

            copado__Promotion__c dev2uat = CopadoTestDataFactory.createForwardPromotion(project.Id, sourceEnv2.Id, uatEnv.Id);
            dev2uat.copado__Status__c = Constants_Core.successfulPromotionStatus;

            copado__Promotion__c dev3uat = CopadoTestDataFactory.createForwardPromotion(project.Id, sourceEnv3.Id, uatEnv.Id);
            dev3uat.copado__Status__c = Constants_Core.successfulPromotionStatus;

            //successful backward promotions
            copado__Promotion__c uatDev2 = CopadoTestDataFactory.createForwardPromotion(project.Id , uatEnv.Id, sourceEnv2.Id);
            uatDev2.copado__Back_Promotion__c = true;
            uatDev2.copado__Status__c = Constants_Core.successfulPromotionStatus;

            copado__Promotion__c uatDev3 = CopadoTestDataFactory.createForwardPromotion(project.Id , uatEnv.Id, sourceEnv3.Id);
            uatDev3.copado__Back_Promotion__c = true;
            uatDev3.copado__Status__c = Constants_Core.successfulPromotionStatus;

            copado__Promotion__c stageUat = CopadoTestDataFactory.createForwardPromotion(project.Id, stageEnv.Id, uatEnv.Id);
            stageUat.copado__Back_Promotion__c = true;
            stageUat.copado__Status__c = Constants_Core.successfulPromotionStatus;


            //failed promotions for other stories. Those should not create any mass back promotion elements
            //successful prod to stage. should not be considered for logic, as one step "too far" (dev - uat - stage - prod)
            copado__Promotion__c prodStage = CopadoTestDataFactory.createForwardPromotion(project.Id, prodEnv.Id, stageEnv.Id);
            prodStage.copado__Back_Promotion__c = true;
            prodStage.copado__Status__c = Constants_Core.successfulPromotionStatus;

            //successful stage to uat validation. as only validation, this should not be considered.
            copado__Promotion__c stageUatValidation = CopadoTestDataFactory.createForwardPromotion(project.Id, stageEnv.Id, uatEnv.Id);
            stageUatValidation.copado__Back_Promotion__c = true;
            stageUatValidation.copado__Status__c = Constants_Promotion.successfulValidationStatus;

            //failed scratch to uat validation. as failed validation, this should not be considered
            copado__Promotion__c scratchUatFailedValidation = CopadoTestDataFactory.createForwardPromotion(project.Id, scratchSource.Id, uatEnv.Id);
            scratchUatFailedValidation.copado__Back_Promotion__c = false;
            scratchUatFailedValidation.copado__Status__c = Constants_Promotion.failedValidationStatus;

            successfulPromotions.add(dev1uat);
            successfulPromotions.add(dev2uat);
            successfulPromotions.add(dev3uat);
            successfulPromotions.add(stageUat);
            successfulPromotions.add(uatDev2);
            successfulPromotions.add(uatDev3);
            successfulPromotions.add(prodStage);
            successfulPromotions.add(stageUatValidation);
            successfulPromotions.add(scratchUatFailedValidation);
            upsert successfulPromotions;

            Map<Id, copado__Promotion__c> previousPromotions = new Map<Id, copado__Promotion__c>([SELECT Id FROM copado__Promotion__c]);

            List<copado__Promoted_User_Story__c> promotedUserStories = new List<copado__Promoted_User_Story__c>();
            //link stories to promotions
            //stage has one of those stories which is back promoted
            promotedUserStories.addAll( CopadoTestDataFactory.createPromotedUserStories(stageUat.Id, new List<Id>{userStory4.Id}) );

            //Dev 1 lacks 3 stories, so only story 1 is forward promoted
            promotedUserStories.addAll( CopadoTestDataFactory.createPromotedUserStories(dev1uat.Id, new List<Id>{userStory1.Id}) );

            //Dev 2 lacks 2 stories
            promotedUserStories.addAll( CopadoTestDataFactory.createPromotedUserStories(dev2uat.Id, new List<Id>{userStory2.Id}) );
            promotedUserStories.addAll( CopadoTestDataFactory.createPromotedUserStories(uatDev2.Id, new List<Id>{userStory3.Id}) );

            //Dev 3 is fully back promoted
            promotedUserStories.addAll( CopadoTestDataFactory.createPromotedUserStories(dev3uat.Id, new List<Id>{userStory3.Id}) );
            promotedUserStories.addAll( CopadoTestDataFactory.createPromotedUserStories(uatDev3.Id, new List<Id>{userStory1.Id}) );
            promotedUserStories.addAll( CopadoTestDataFactory.createPromotedUserStories(uatDev3.Id, new List<Id>{userStory2.Id}) );
            promotedUserStories.addAll( CopadoTestDataFactory.createPromotedUserStories(uatDev3.Id, new List<Id>{userStory4.Id}) );

            /**
            * link surrounding stories to failed promotions or validations
            */
            //firs successful promotion from prod to stage
            promotedUserStories.addAll( CopadoTestDataFactory.createPromotedUserStories(prodStage.Id, new List<Id>{prodEnvStory.Id}) );

            //successful validation from stage to UAT
            promotedUserStories.addAll( CopadoTestDataFactory.createPromotedUserStories(stageUatValidation.Id, new List<Id>{prodEnvStory.Id}) );

            //failed validation from scratch to UAT
            promotedUserStories.addAll( CopadoTestDataFactory.createPromotedUserStories(scratchUatFailedValidation.Id, new List<Id>{scratchSourceStory.Id}) );

            insert promotedUserStories;
            Map<Id, copado__Promoted_User_Story__c> previousPUS= new Map<Id, copado__Promoted_User_Story__c>([SELECT Id FROM copado__Promoted_User_Story__c]);

            excludeEnvironments.add(scratchSource.Id);


            Test.startTest();


            CopadoMassBackPromoteHandler mbph = new CopadoMassBackPromoteHandler();
            mbph.createTargetOrgMap(flow.Id, stageEnv, uatEnv, excludeEnvironments ,1);

            Test.stopTest();

            //check Stories
            for(copado__User_Story__c us: [SELECT Id, Name, copado__Environment__r.Name, copado__Promote_Change__c FROM copado__User_Story__c]){
                System.debug('Name: ' +  us.Name + ' | Env: ' + us.copado__Environment__r.Name + ' | Promote? ' + us.copado__Promote_Change__c);
            }
            //check promotions
            List<copado__Promotion__c> newPromotions = [
                    SELECT
                            Id,
                            Name,
                            copado__Source_Org_Credential__c,
                            copado__Source_Org_Credential__r.Name,
                            copado__Source_Environment__c,
                            copado__Source_Environment__r.Name,
                            copado__Destination_Org_Credential__c,
                            copado__Destination_Org_Credential__r.Name,
                            copado__Destination_Environment__c,
                            copado__Destination_Environment__r.Name,
                            copado__Back_Promotion__c,
                            copado__Status__c
                    FROM
                            copado__Promotion__c
                    ORDER BY Name ASC, copado__Source_Environment__c

            ];

            for(copado__Promotion__c prom : newPromotions){

                String start;
                if(previousPromotions.keySet().contains(prom.Id)){
                    start = 'Those promotions are old: ';
                } else{
                    start = 'Those promotions are new: ';
                }

                System.debug(
                        start
                        + prom.Name +  ' - '
                        + prom.copado__Source_Environment__r.Name +  ' - '
                        + prom.copado__Destination_Environment__r.Name +  ' - '
                        + prom.copado__Back_Promotion__c +  ' - '
                        + prom.copado__Status__c
                );
            }

            //check promoted User Stories
            List<copado__Promoted_User_Story__c> testPus = new List<copado__Promoted_User_Story__c>([
                    SELECT
                            Id,
                            Name,
                            copado__User_Story__c,
                            copado__User_Story__r.Name,
                            copado__Promotion__c,
                            copado__Promotion__r.copado__Source_Environment__r.Name,
                            copado__Promotion__r.copado__Destination_Environment__r.Name,
                            copado__Promotion__r.copado__Back_Promotion__c,
                            copado__Promotion__r.copado__Status__c
                    FROM
                            copado__Promoted_User_Story__c
                    ORDER BY copado__User_Story__r.Name ASC, copado__Promotion__r.copado__Source_Environment__c
            ]);


            for(copado__Promoted_User_Story__c pus: testPus){

                String start;
                if(previousPUS.keySet().contains(pus.Id)){
                    start = 'Those PUS are old: ';
                } else{
                    start = 'Those PUS are new: ';
                }

                System.debug(
                        start
                        //+ pus.Name +  ' - '
                        + pus.copado__User_Story__r.Name +  ' - '
                        + pus.copado__Promotion__r.copado__Source_Environment__r.Name +  ' - '
                        + pus.copado__Promotion__r.copado__Destination_Environment__r.Name +  ' - '
                        + pus.copado__Promotion__r.copado__Back_Promotion__c +  ' - '
                        + pus.copado__Promotion__r.copado__Status__c
                );
            }
        }
    }
}