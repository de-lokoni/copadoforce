/**
 * Created by kheidt on 17/04/2018.
 * to invoque a promotion deployment through another apex class: CopadoPromotionService.createAndDeployForwardPromotion('NameOfEnvironment', 'NameOfProject');
 * to invoque a promotion validation through another apex class: CopadoPromotionService.createAndValidateForwardPromotion('NameOfEnvironment', 'NameOfProject');
 */

public with sharing class CopadoForwardPromotionHandler {

    public copado__Promotion__c queriedPromotion = new copado__Promotion__c();
    public Boolean hasApexCode = false;
    public CopadoEnvironmentService ces;
    public CopadoProjectService cprs;
    public CopadoPromotionService cpms;

    /**
     * create and deploy a promotion UP the flow. parameters are the name of an environment and a project
     *
     * @param sourceEnvName
     * @param prjName
     */
    public void createAndDeployForwardPromotion(
            String sourceEnvName,
            String prjName
    ){

        Id sourceEnvId = CopadoEnvironmentService.getEnvironementByName(sourceEnvName).Id;
        Id projectId = CopadoProjectService.getProjectByName(prjName).Id;
        String testlevel = hasApexCode == true ? Constants_Core.runSelectedTests : Constants_Core.noTestRun;

        createDeployOrValidatePromotionBase(
                sourceEnvId,
                projectId,
                testLevel,
                false
        );
    }

    /**
     * create and validate validation UP the flow. parameters are the name of an environment and a project
     *
     * @param sourceEnvName
     * @param prjName
     */
    public void createAndValidateForwardPromotion(
            String sourceEnvName,
            String prjName
    ){

        Id sourceEnvId = CopadoEnvironmentService.getEnvironementByName(sourceEnvName).Id;
        Id projectId = CopadoProjectService.getProjectByName(prjName).Id;
        String testlevel = hasApexCode == true ? Constants_Core.runSelectedTests : Constants_Core.noTestRun;

        createDeployOrValidatePromotionBase(
                sourceEnvId,
                projectId,
                testLevel,
                true
        );
    }


    /**
     * Main method to create the promotion, add stories and fire or validate it
     *
     * @param sourceEnvId
     * @param projectId
     * @param testLevel
     * @param isValidation
     */

    public void createDeployOrValidatePromotionBase(
            Id sourceEnvId,
            Id projectId,
            String testLevel,
            Boolean isValidation
    ){
        copado__Promotion__c promotion = CopadoPromotionService.createForwardPromotion(sourceEnvId, projectId);

        insert promotion;

        //just to avoid re-querying
        queriedPromotion = promotion;

        List<copado__Promoted_User_Story__c> pus = CopadoPromotionService.attachUserStoriesBasedOnProject(promotion.Id);
        insert pus;
        CopadoPromotionService.deployPromotion(promotion.Id,testLevel,isValidation, false);
        //return promotion;
    }
}