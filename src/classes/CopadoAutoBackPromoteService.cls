/**
 * Created by kheidt on 07/09/2018.
 */

public with sharing class CopadoAutoBackPromoteService {

    public Set<Id> envIds = new Set<Id>();
    public copado__Promotion__c finishedPromotion;
    public List<Id> newPromotionIds = new List<Id>();

    //mimic standard Constructor
    public CopadoAutoBackPromoteService(){}

    //Constructor to be called by a trigger
    public CopadoAutoBackPromoteService(
            copado__Promotion__c inputPromotion
    ){
        finishedPromotion = inputPromotion;
    }

    public void createBackPromotionsForLowerOrgs(){

        this.getTargetOrgIds();
        this.createNewPromotions();
        this.addStoriesFromOtherPromotions();
        this.fireDeploymentCreationForNewPromotions();
    }

    //check for lower orgs
    public void getTargetOrgIds() {
        //reuse code to get the environment record
        finishedPromotion = CopadoPromotionService.getPromotion(finishedPromotion.Id);
        System.debug('Promotion finished: Id: ' + finishedPromotion.Id + ' | source ' + finishedPromotion.copado__Source_Environment__c  + ' | destination ' + finishedPromotion.copado__Destination_Environment__c  + ' | ' + finishedPromotion.copado__Back_Promotion__c);
        copado__Environment__c destEnvironment = CopadoEnvironmentService.getEnvironmentByOrgId(
                finishedPromotion.copado__Destination_Org_Credential__c
        );
        System.debug('This is the finished promotion target environment: ' + destEnvironment);
        Boolean isBackPromotion = finishedPromotion.copado__Back_Promotion__c;
        Id pipelineId = finishedPromotion.copado__Project__r.copado__Deployment_Flow__c;
        System.debug('pipeline Id: ' + pipelineId);
        System.debug('is back promotion: ' + isBackPromotion);
        /**
        * A successful promotion could be coming from a lower environment, or as a successful back promotion from above
        * If the promotion is coming from above the logic should create back promotions for all lower environments.
        * If the promotion is coming from below, the logic should create back promotions for all other lower environments
        */
        List<copado__Deployment_Flow_Step__c> flowSteps = CopadoFlowStepService.getPotentialEnvironmentSteps(destEnvironment, pipelineId);

        System.debug('steps found: ' + flowSteps.size());

        for(copado__Deployment_Flow_Step__c step : flowSteps){
            System.debug('This is the current Flow Step: ' + step);

            //for back promotions
            if(isBackPromotion){
                //add all environments to the list where the destination environment of the back-promotion is the destination environment of the step
                if(
                    step.copado__Destination_Environment__c == finishedPromotion.copado__Destination_Environment__c &&
                    step.copado__Source_Environment__c != finishedPromotion.copado__Destination_Environment__c
                ){
                envIds.add(step.copado__Source_Environment__c);
                System.Debug('BP: Environment Added: ' + step.copado__Source_Environment__r.Name);
                }
            }
            //for forward promotions add ids of source env id's
            else if(
                    step.copado__Source_Environment__c != finishedPromotion.copado__Source_Environment__c &&
                    step.copado__Destination_Environment__c == finishedPromotion.copado__Destination_Environment__c
            ) {
                if(step.copado__Source_Environment__r.copado__Type__c != Constants_Core.scratchOrgType) {
                    this.envIds.add(step.copado__Source_Environment__c);
                    System.Debug('FP: Environment Added: ' + step.copado__Source_Environment__r.Name);
                }
            }
        }
    }

    //create back promotion for all lower orgs
    public void createNewPromotions() {
        List<copado__Promotion__c> newPromotions = CopadoPromotionService.createBackwardPromotions(
                new List<Id>(this.envIds), this.finishedPromotion.copado__Project__c
        );
        System.debug('we will create the following new promotions' + newPromotions.size() + ' | ' + newPromotions);
        insert newPromotions;

        for(copado__Promotion__c prom : newPromotions){
            newPromotionIds.add(prom.Id);
        }

    }

    //clone user stories from original promotion to new promotions
    public void addStoriesFromOtherPromotions(){
        List<copado__Promoted_User_Story__c> newPus = CopadoPromotionService.attachStoriesFromPromotions(
                new List<Id>{finishedPromotion.Id},
                newPromotionIds
        );
        insert newPus;
    }

    //fire promotion deployment creation
    public void fireDeploymentCreationForNewPromotions(){

        //by the time this is executed, the User Story Service should have run the lines to check if stories have apex code.
        String testlevel = CopadoUserStoryService.hasApexCode == true ? Constants_Core.runSelectedTests : Constants_Core.noTestRun;

        for(Id promoId : newPromotionIds){
            CopadoPromotionService.deployPromotion(
                    String.valueOf(promoId),
                    testlevel,
                    true,
                    false
            );
        }
    }
}