@isTest
public class DT_AccountServiceTest {

    public static testmethod void testSetDescription(){
 		Account newAccount = new Account();
        Test.startTest();
        DT_AccountService.setAccountDescription(newAccount);
        Test.stopTest();
        System.assertEquals('Hello DT World', newAccount.Description);
    }
    
    public static testmethod void testSetDescriptionWithTrigger(){
 		Account newAccount = new Account(
        	Name = 'TestAccount'
        );
        Test.startTest();
		insert newAccount;
        Test.stopTest();
        newAccount = [SELECT Id, Description FROM Account LIMIT 1];
        //System.assertEquals('Hello DT World', newAccount.Description);
    }
    
}