/**
 * Created by kheidt on 2019-07-23.
 */

public with sharing class PromotionMessageServiceInvocable {
    @InvocableMethod(Label='Send Email After Promotion' description='Sends an email to a default address depending on the environment containing the list of stories for a Promotion.')
    public static void sendEmailAfterSuccessfulPromotion(List<Id> promotionIds){

        PromotionMessageService pms = new PromotionMessageService(promotionIds[0]);
        pms.sendMessageAfterSuccessfulPromotion();
    }
}