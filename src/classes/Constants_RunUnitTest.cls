/**
 * Created by kheidt on 2019-05-30.
 */

public with sharing class Constants_RunUnitTest {
    public static final String runTestsOnStoryBase = '/json/v1/webhook/apexTest/';
}