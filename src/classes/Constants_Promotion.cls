/**
 * Created by kheidt on 2019-06-07.
 */

public with sharing class Constants_Promotion {

    public static final String successfulPromotionStatus = 'Completed';
    public static final String failedPromotionStatus = 'Completed with errors';
    public static final String successfulValidationStatus = 'Validated';
    public static final String failedValidationStatus = 'Validation failed';
    public static final String notStartedPromotion = 'Draft';

}