Public Class MyCopadoService{


    /**
     * Hi, Dev1 here, i provide more comments to explain the functionality 
     **/
     
     /**
     hi, this is a comment for user story 1
     **/
    
    public void myCopadoServiceMethod(){
        System.Debug('Hello Copado');

        List<Account> accts = new List<Account>();
        for(Integer i = 0; i<10; i++){
            accts = [SELECT Id FROM Account WHERE Name LIKE 'i%'];
        }

        /** Dev 2 change: this thing is not required        
        if ( accts != null && accts.size() > 0){
            upsert accts;
        }
        **/
    
    }   
    
    /**
    hi this is for story 2. please don't include me in story 1
    **/
}