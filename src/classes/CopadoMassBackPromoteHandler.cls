/**
 * Created by kheidt on 2019-06-03.
 */

public with sharing class CopadoMassBackPromoteHandler {

    //main method
    public void createTargetOrgMap(
            Id deploymentFlowId,
            copado__Environment__c currentEnvironment,
            copado__Environment__c mergeEnvironment,
            Set<Id> excludedEnvironments,
            Integer pastDays
    ){

        if(mergeEnvironment == null){
            mergeEnvironment = currentEnvironment;
        }

        if(excludedEnvironments == null){
            excludedEnvironments = new Set<Id>();
        }
        //we need a project Id for later on constructing the promotions
        String flowId;

        /**
        * get the environments before and after the current environment
        * TODO: move to deployment flow step service
        **/
        List<copado__Deployment_Flow_Step__c> stepList = [
                SELECT
                        Id,
                        Name,
                        copado__Source_Environment__c,
                        copado__Destination_Environment__c,
                        copado__Deployment_Flow__c,
                        copado__Branch__c,
                        copado__Destination_Branch__c
                FROM copado__Deployment_Flow_Step__c
                WHERE
                        copado__Deployment_Flow__c = :deploymentFlowId AND
                        copado__Source_Environment__c NOT IN :excludedEnvironments AND
                        copado__Source_Environment__r.copado__Type__c != 'Scratch Org' AND
                        (
                            copado__Destination_Environment__c = :mergeEnvironment.Id
                            OR
                            copado__Destination_Environment__c = :currentEnvironment.Id
                            OR
                            copado__Source_Environment__c =:currentEnvironment.Id
                        )
        ];

        Set<String> targetEnvIds =  new Set<String>();
        Id nextEnvId;
        Id previousEnvId;

        /**
        * split the list to have a set of unique lower environment id's which will be the target environments for promotions
        * get the next higher environment to query higher originated stories
        */
        for(copado__Deployment_Flow_Step__c dfs : stepList){

            System.debug('Flow step: ' + dfs);
            //split the future targets for back promotions first
            if(dfs.copado__Destination_Environment__c == mergeEnvironment.Id) {
                targetEnvIds.add(dfs.copado__Source_Environment__c);
            }
            //if not, let's get potential stories from higher environments than the current one
            else if(dfs.copado__Source_Environment__c == currentEnvironment.Id){
                nextEnvId = dfs.copado__Destination_Environment__c;
            }
            //also, we need the org before the current environment, just in case
            else if(dfs.copado__Destination_Environment__c == currentEnvironment.Id){
                previousEnvId = dfs.copado__Source_Environment__c;
            }
            //If there's something else that has been queried:
            else{
                System.debug('This step is strange: ' + dfs);
            }

            //required to get a project. doesn't matter which as long as it is connected to the right flow.
            if(flowId == null){
                flowId = dfs.copado__Deployment_Flow__c;
            }
        }

        /**
        * TODO: move to project service
        */
        String projectId = [SELECT Id FROM copado__Project__c WHERE copado__Deployment_Flow__c = :flowId LIMIT 1].Id;

        /**
         * We need the following promoted user stories:
         * forward promoted work in progress: source = lower orgs & target = current org
         * backward promoted work in progress: source = current org & target  = lower orgs
         * backward promoted from higher orgs: source = next env & target = current org
         * TODO: create service
         */

        List<copado__Promoted_User_Story__c> promotedStoriesInScope = [
                SELECT
                        Id,
                        copado__User_Story__c,
                        copado__User_Story__r.Name,
                        copado__User_Story__r.copado__Promote_Change__c,
                        copado__Promotion__c,
                        copado__Promotion__r.copado__Source_Environment__c,
                        copado__Promotion__r.copado__Source_Environment__r.Name,
                        copado__Promotion__r.copado__Destination_Environment__c,
                        copado__Promotion__r.copado__Destination_Environment__r.Name,
                        copado__Promotion__r.copado__Back_Promotion__c,
                        copado__Promotion__r.copado__Status__c

                FROM copado__Promoted_User_Story__c
                WHERE
                copado__Promotion__r.copado__Status__c = :Constants_Core.successfulPromotionStatus
                AND copado__User_Story__r.copado__Exclude_From_CBM__c = FALSE
                //AND copado__User_Story__r.copado__Promote_Change__c = TRUE
                AND copado__User_Story__r.copado__Environment__c = :currentEnvironment.Id
                AND(
                    (
                        //forward promoted to merge org
                        copado__Promotion__r.copado__Source_Environment__c IN : targetEnvIds AND
                        copado__Promotion__r.copado__Destination_Environment__c = :mergeEnvironment.Id
                    )
                    OR (
                        //backward promoted from merge org
                        copado__Promotion__r.copado__Back_Promotion__c = true AND
                        copado__Promotion__r.copado__Source_Environment__c = :mergeEnvironment.Id AND
                        copado__Promotion__r.copado__Destination_Environment__c IN :targetEnvIds
                    )
                    OR (
                        //backward promoted to current org
                        copado__Promotion__r.copado__Back_Promotion__c = true AND
                        copado__Promotion__r.copado__Source_Environment__c = :nextEnvId AND
                        copado__Promotion__r.copado__Destination_Environment__c = :currentEnvironment.Id
                    )
                        //stories promoted regularly to current org
                        //stories originated in current Environment but not yet promoted anywhere
                )
                ORDER BY copado__User_Story__c ASC
        ];

        /**
        * split the list to get stories which are missing for certain environments
        */

        //this map will store where a story still needs to be deployed to
        Map<String, Set<String>> environmentsPerStory = new Map<String, Set<String>>();

        //this map is the target outcome which will be used to create promotions
        Map<String, Set<String>> storiesPerEnvironment = new Map<String, Set<String>>();

        /**
        * time to loop through the promoted stories and find out where they were
        * and where they still need to be back promoted to
        */
        for(copado__Promoted_User_Story__c pus : promotedStoriesInScope){

            //instanciate map with all target environments as promotion target, if required
            if(environmentsPerStory.get(pus.copado__User_Story__c) == null){
                environmentsPerStory.put(pus.copado__User_Story__c, new Set<String>(targetEnvIds));
            }
            //get the set out of the map
            Set<String> loopTargetEnv = environmentsPerStory.get(pus.copado__User_Story__c);

            System.debug('Old Target Env: ' + loopTargetEnv);
            //story was originated in one of the targets and deployed to current env
            if(
                    targetEnvIds.contains(pus.copado__Promotion__r.copado__Source_Environment__c) &&
                    pus.copado__Promotion__r.copado__Destination_Environment__c == currentEnvironment.Id
            ){
                loopTargetEnv.remove(pus.copado__Promotion__r.copado__Source_Environment__c);
            }
            //story was back-promoted to one of the targets from current enc
            else if(
                    pus.copado__Promotion__r.copado__Back_Promotion__c == true &&
                    targetEnvIds.contains(pus.copado__Promotion__r.copado__Destination_Environment__c) &&
                    pus.copado__Promotion__r.copado__Source_Environment__c == currentEnvironment.Id
            ){
                loopTargetEnv.remove(pus.copado__Promotion__r.copado__Destination_Environment__c);
            }

            System.debug('Pus Analysis: ' +
                    ' - PUS ID: ' + pus.Id +
                    ' - US Number: ' + pus.copado__User_Story__r.Name +
                    ' - US Promote Change: ' + pus.copado__User_Story__r.copado__Promote_Change__c +
                    ' - Source: ' + pus.copado__Promotion__r.copado__Source_Environment__r.Name +
                    ' - Target: ' + pus.copado__Promotion__r.copado__Destination_Environment__r.Name +
                    ' - Back?: ' + pus.copado__Promotion__r.copado__Back_Promotion__c +
                    ' - Status: ' + pus.copado__Promotion__r.copado__Status__c +
                    ' - new target environments: ' + loopTargetEnv
            );
            environmentsPerStory.put(pus.copado__User_Story__c, null);
            environmentsPerStory.put(pus.copado__User_Story__c, loopTargetEnv);
        }

        /**
        * now that we know where a story has been and where it still needs to go
        * we need to create a env - [stories] map, so that we can create the promotions based on that
        */

        for(String story : environmentsPerStory.keySet()){
            //get list of environments
            Set<String> loopTargetEnv = environmentsPerStory.get(story);
            for(String env : loopTargetEnv){

                //instanciate if required
                if(storiesPerEnvironment.get(env) == null){
                    storiesPerEnvironment.put(env, new Set<String>());
                }

                storiesPerEnvironment.get(env).add(story);
            }
        }

        /**
        * we have a map of target env - [stories], so this will allow us to create the promotions
        */


        List<copado__Promotion__c> newBackPromotions = new List<copado__Promotion__c>();
        List<copado__Promoted_User_Story__c> newPus = new List<copado__Promoted_User_Story__c>();


        for(String env : storiesPerEnvironment.keySet()){

            //create back promotion
            copado__Promotion__c promotion = CopadoPromotionService.createBackPromotion(env,projectId );

            //add to list
            newBackPromotions.add(promotion);
            System.debug('This is my new Promotion: ' + promotion);
        }

        //dml to get the Ids
        upsert newBackPromotions;

        //Query promotions for debugging. TODO: Remove this query when done
        Map<Id, copado__Promotion__c> newPromotionMap = new Map<Id, copado__Promotion__c>([
                SELECT Id, Name, copado__Source_Environment__r.Name, copado__Destination_Environment__r.Name
                FROM copado__Promotion__c
        ]);

        //now create the
        for(copado__Promotion__c promotion : newBackPromotions){

            //get the stories which need to be deployed to a certain target environment
            for(String usId : storiesPerEnvironment.get(promotion.copado__Destination_Environment__c)){

                //add stories to promotion
                newPus.add(new copado__Promoted_User_Story__c(
                        Name = promotion.copado__Destination_Environment__c + '_' + usId,
                        copado__Promotion__c = promotion.Id,
                        copado__User_Story__c = usId
                ));
                //System.Debug('This');
            }

        }

        upsert newPus;

        //TODO: make sure to consider test classes at some point

        for(copado__Promotion__c promotion : newBackPromotions){
            CopadoPromotionService.deployPromotion(
                    String.valueOf(promotion.Id),
                    Constants_Core.noTestRun,
                    true,
                    false
            );
        }
    }
}