/**
 * Created by kheidt on 2019-07-23.
 */

public with sharing class PromotionMessageService {

    //constructors
    public PromotionMessageService(){}
    public PromotionMessageService(Id promotionId){
        successfulPromotion = promotionId;
    }

    //key variables
    public Id successfulPromotion;
    @TestVisible private Messaging.SingleEmailMessage promotionMail;

    //get related Promoted User Stories:

    public void sendMessageAfterSuccessfulPromotion(){

        //double check, if main input var exists
        if(successfulPromotion == null){
            System.debug(LoggingLevel.INFO, 'The promotion ID is null' );
            return;
        }

        //get promotion with details
        copado__Promotion__c prom = this.getPromotion(successfulPromotion);

        //get user stories of the promotion with some more details
        CopadoPromotedStoriesService cpss = new CopadoPromotedStoriesService();
        List<copado__Promoted_User_Story__c> promotionPUS = cpss.getStoriesOfAPromotion(successfulPromotion);

        //get default adresses for the target environment

        String recipients = prom.copado__Destination_Environment__r.DefaultPromotionNotificationMail__c;

        //construct html email body
        String emailBody = this.constructEmailBody(promotionPUS, prom);

        try {
            //prepare email
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(recipients.split(','));
            mail.setCcAddresses(new List<String>{
                    UserInfo.getUserEmail()
            });
            mail.setSubject('Successful Copado Promotion to: ' + prom.copado__Destination_Environment__r.Name);
            mail.setHtmlBody(emailBody);

            this.promotionMail = mail;

            //send email
            Messaging.sendEmail(new List<Messaging.Email>{
                    mail
            });
        } catch(exception e) {
            System.Debug(LoggingLevel.INFO, 'Something went wrong sending the mail: ' + e);
            System.Debug(LoggingLevel.INFO,  e.getCause());
            System.Debug(LoggingLevel.INFO,  e.getStackTraceString());
            throw  e;
        }

    }

    private String constructEmailBody(List<copado__Promoted_User_Story__c> promotedStories, copado__Promotion__c p){

        String baseURL = System.URL.getSalesforceBaseUrl().toExternalForm() + '/';
        String htmlBody = '';

        htmlBody += 'Your Promotion ' + p.Name + ' has been successfully deployed.<p><p>';
        htmlBody += 'To view your Promotion <a href=' + baseURL + p.Id +'>click here.</a> <p><p>';


        //open table..
        htmlBody += '<table border="1" style="border-collapse: collapse">' +
                '<caption>Promoted User Stories</caption>' +
                '<tr><th>Project</th><th>Release</th><th>Story Number</th><th>Story Title</th></tr>';

        //iterate over list and output columns/data into table rows...
        for(copado__Promoted_User_Story__c pus : promotedStories){

            String project = pus.copado__User_Story__r.copado__Project__r.Name;
            if(pus.copado__User_Story__r.copado__Project__r.Name == null){project= '[Not Provided]';}

            String release = pus.copado__User_Story__r.copado__Release__r.Name;
            if(pus.copado__User_Story__r.copado__Release__r.Name == null){release = '[Not Provided]';}

            String storyNumber = pus.copado__User_Story__r.Name;

            String title = pus.copado__User_Story__r.copado__User_Story_Title__c;
            if(pus.copado__User_Story__r.copado__User_Story_Title__c == null){title = '[Not Provided]';}



            htmlBody += '<tr>' +
                    '<td>' + project + '</td>' +
                    '<td>' + release + '</td>' +
                    '<td>' + storyNumber + '</td>' +
                    '<td>' + title + '</td>' +
                    '</tr>';

        }
        //close table...
        htmlBody += '</table><p>';

        htmlBody += 'Kind regards, ' +
                '<p> your Release Team';
        return htmlBody;

    }

    private copado__Promotion__c getPromotion(Id promotionId){
        return [
                SELECT
                        Id,
                        Name,
                        copado__Destination_Environment__c,
                        copado__Destination_Environment__r.Name,
                        copado__Destination_Environment__r.EnvironmentPurpose__c,
            			copado__Destination_Environment__r.DefaultPromotionNotificationMail__c,
                        copado__Source_Environment__c,
                        copado__Source_Environment__r.Name,
                        copado__Source_Environment__r.EnvironmentPurpose__c,
                        copado__Status__c
                FROM
                        copado__Promotion__c
                WHERE Id = :successfulPromotion
        ];
    }

}