/**
 * Created by kheidt on 2019-05-30.
 */

public with sharing class CopadoRunTestHelper {
    /**
     *
     *
     * @param usIds
     */
    @InvocableMethod
    public static void runUnitTestsOnStory(List<String> usIds){
        if(!usIds.isEmpty() || usIds != null) {
            String usId = usIds[0];

            fireWebhook('GET',constructWebhookURL(usId),'');
        }
    }

    @Future(callout=true)
    public static void fireWebhook(String type, String webhookURL, String webhookBody){
        CopadoWebhookService.callWebhook(type, webhookURL, webhookBody);
    }

    private static String constructWebhookURL(String usId){

        //construct URL parameters
        String apiParameter = 'api_key=' + Constants_Core.apiKey;
        String usIdParameter = 'userStoryId=' + usId;

        //Construct Endpoint URL
        String webhookURL = Constants_Core.webhookBase + Constants_RunUnitTest.runTestsOnStoryBase;
        webhookURL += usIdParameter;
        webhookURL += '?' + apiParameter;

        System.debug('endpoint: ' + webhookURL);

        return webhookURL;
    }

}