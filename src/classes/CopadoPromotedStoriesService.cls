/**
 * Created by kheidt on 2019-07-23.
 */

public with sharing class CopadoPromotedStoriesService {

    public List<copado__Promoted_User_Story__c> getStoriesOfAPromotion(Id promotionId){

        List<copado__Promoted_User_Story__c> promotedStories = new List<copado__Promoted_User_Story__c>();

        promotedStories = [
                SELECT
                        Id,
                        copado__User_Story__c,
                        copado__User_Story__r.Name,
                        copado__User_Story__r.copado__User_Story_Title__c,
                        copado__User_Story__r.copado__Project__r.Name,
                        copado__User_Story__r.copado__Release__r.Name,
                        copado__User_Story__r.copado__Sprint__r.Name,
                        copado__User_Story__r.copado__Epic__r.Name,
                        copado__User_Story__r.Owner.Name,
                        copado__User_Story__r.copado__Developer__r.Name,
                        copado__User_Story__r.copado__Test_Script_Owner__r.Name
                FROM
                        copado__Promoted_User_Story__c
                WHERE copado__Promotion__c = :promotionId
        ];

        return promotedStories;

    }

}