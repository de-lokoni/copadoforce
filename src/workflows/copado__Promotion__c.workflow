<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Promotion_Rejected</fullName>
        <description>Promotion Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Promotion_Approval</template>
    </alerts>
    <fieldUpdates>
        <fullName>Set_to_Draft</fullName>
        <field>copado__Status__c</field>
        <literalValue>Draft</literalValue>
        <name>Set to Draft</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_to_Rejected</fullName>
        <field>copado__Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Set to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_to_approved</fullName>
        <field>copado__Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Set to approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_to_waiting_for_approval</fullName>
        <field>copado__Status__c</field>
        <literalValue>Waiting For Approval</literalValue>
        <name>Set to waiting for approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
</Workflow>
